require('user.options')
require('user.plugins')
require('user.colorscheme')
require('user.keymaps')

-- Plugins Config
require('user.telescope')
require('user.indentblankline')
require('user.treesitter')
-- require('user.cmp')

-- LSP Config
require'lspconfig'.tsserver.setup{}

require('user.lsp')
