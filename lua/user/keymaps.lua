-- How to configure a keymap using the VIM's api for Lua
-- vim.api.nvim_set_keymap("n", "<C-p>", ":Ex<CR>", { noremap = true, silent = true })

local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap
-- Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

keymap("n", "<leader>p", ":Ex<CR>", opts)

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Telescope config using Lua functions
keymap("n", "<leader>tf", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<leader>tg", "<cmd>Telescope live_grep<cr>", opts)
keymap("n", "<leader>tb", "<cmd>Telescope buffers<cr>", opts)
keymap("n", "<leader>th", "<cmd>Telescope help_tags<cr>", opts)
keymap("n", "<C-p>", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<C-n>", "<cmd>Telescope buffers<cr>", opts)

-- Delete all buffers but the current one
keymap("n", "<leader>bda", "<cmd>%bd|e#<cr>", opts)
-- Delete current buffer
keymap("n", "<leader>bdc", "<cmd>bdelete<cr>", opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)
