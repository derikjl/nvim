local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

return packer.startup(function(use)
  use 'wbthomason/packer.nvim' -- Packer can manage itself
  use 'neovim/nvim-lspconfig' -- Collection of configurations for the built-in LSP client

  -- CMP Code Completion
  -- use 'onsails/lspkind.nvim'
  -- use {
  --   "hrsh7th/nvim-cmp",
  --   requires = {
  --     "hrsh7th/cmp-buffer", 
  --     "hrsh7th/cmp-nvim-lsp",
  --     -- 'L3MON4D3/LuaSnip',
  --     -- 'saadparwaiz1/cmp_luasnip',
  --     'octaltree/cmp-look', 
  --     'hrsh7th/cmp-path', 
  --     'f3fora/cmp-spell', 
  --     'hrsh7th/cmp-emoji'
  --   }
  -- }

  use 'christoomey/vim-tmux-navigator'

  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- tree-sitter | syntax highlighter
  use 'nvim-treesitter/nvim-treesitter'

  -- show line indentation / spaces
  use 'lukas-reineke/indent-blankline.nvim'

  use {'dracula/vim', as = 'dracula'} -- Theme
end)
