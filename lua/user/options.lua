-- How configure vim using Lua syntax
-- vim.opt['number'] = true

local options = {
  number = true,
  relativenumber = true,
  cursorline = true,
  signcolumn = "yes",
  autoindent = true,
  smartindent = true,

  -- tabs
  expandtab = true,
  tabstop = 2,
  softtabstop = 2,
  shiftwidth = 2,

  -- search
  incsearch = true,

  wrap = false,
  scrolloff = 8,
  sidescrolloff = 8,
  smartcase = true,
  smartindent = true,
  splitbelow = true,
  splitright = true,
  syntax = "enable",
  clipboard = "unnamedplus",
  mouse = "a",
  showmode = false,
  termguicolors = true,
  errorbells = false,

  writebackup = false,
  undodir = vim.fn.stdpath("config") .. "/undodir",
  undofile = true,
  swapfile = false,

  -- helps with autocompletion later on
  completeopt = 'menuone,noinsert,noselect'
}

for k, v in pairs(options) do
  vim.opt[k] = v
end
